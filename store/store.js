import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        authorized: false,
        loginPair: {token: '', id: 0},
        user: {id: 0, role: '', token: '', authorized: false},
        page: 1
    },
    mutations: {
        setPair (state, payload) {
            state.loginPair = payload;
            localStorage.setItem('pair', JSON.stringify(payload));
        },
        setUser (state, payload) {
            state.user = payload;
            state.authorized = (state.user !== null && state.user.id > 0);
            localStorage.setItem('user', JSON.stringify(payload));
        },
        setPage (state, payload) {
            state.page = payload;
            localStorage.setItem('page', JSON.stringify(payload));
        },
        incPage (state) {
            state.page++;
            localStorage.setItem('page', JSON.stringify(state.page));
        },
        decPage (state) {
            if (state.page > 1) {
                state.page--;
                localStorage.setItem('page', JSON.stringify(state.page));
            }
        },
        clear (state) {
            state = {};
        }
    }
});

//export default store;