import Vue from 'vue';
import VueRouter from 'vue-router';
import {sync} from 'vuex-router-sync'
import {store} from './store/store.js';
import App from './components/App.vue';
import NavBar from './components/NavBar.vue';
import MainMenu from './components/MainMenu.vue';
import Home from './components/Home.vue';
import LoginForm from './components/LoginForm.vue';

Vue.component('nav-bar', NavBar);
Vue.component('main-menu', MainMenu);

Vue.use(VueRouter);

const routes = [
    {path: '/', component: LoginForm},
    {path: '/home', component: Home}
];

const router = new VueRouter({
    routes,
    mode: 'history'
});

sync(store, router);


function defaultUser() {
    return {
        authorized: false,
        id: 0,
        role: '',
        token: '',
    };
}

router.beforeEach((to, from, next) => {
    let user = JSON.parse(localStorage.getItem('user') || JSON.stringify(defaultUser()));
    let pair = JSON.parse(localStorage.getItem('pair') || '{}');
    let page = JSON.parse(localStorage.getItem('page') || '1');
    if (user) {
        store.commit('setUser', user);
    }
    if (pair) {
        store.commit('setPair', pair);
    }
    if (page) {
        store.commit('setPage', page);
    }
    //alert(JSON.stringify(user));
    next();
});


new Vue({
    el: '#app',
    // render: function (createElement) {
    /// 	return createElement(App);
    // }
    router,
    store,
    render: h => h(App)
});
